﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Health : MonoBehaviour {
    public int currentHealth;
    public int maxHealth;
    public abstract void AdjustHealth(int healthAdjustment, int hitStateHash);
    public abstract void AdjustHealth(int healthAdjustment, Vector3 point, int hitStateHash);
    protected abstract void Die();
    public abstract bool IsDamagable();
    public bool dead;
}
