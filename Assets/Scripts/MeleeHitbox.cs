﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.States;
using System;
using System.Collections;

public class MeleeHitbox : MonoBehaviour {

    public LayerMask collisionLayers;
    public GameObject hitParticles;

    List<Transform> hitTargets = new List<Transform>();
    Vector3 prevPos;
    GameStateManager gameStateManager;
    CharacterStateManager characterStateManager;
    Rigidbody myRigidbody;

    void Awake() {
        gameStateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
        characterStateManager = GetComponentInParent<CharacterStateManager>();
        myRigidbody = transform.root.GetComponent<Rigidbody>();
    }

	void OnEnable() {
        prevPos = transform.position;
        hitTargets.Clear();
        if (hitParticles) {
            hitParticles.SetActive(false);
            hitParticles.GetComponent<ParticleSystem>().Clear();
        }
    }

    void Update () {
        CharacterState characterState = characterStateManager.state;
        Vector3 target = prevPos- transform.position;
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, characterState.castSize, target, target.magnitude, collisionLayers);
        if (characterState.isSelfLauncher) {
            gameStateManager.AssignLaunchTarget(myRigidbody, characterStateManager.state.launchSpeed * .8f, characterState.launchTime);
            gameStateManager.IgnoreCollisionWithLaunchedTargets(GetComponent<Collider>());
        }
        foreach ( RaycastHit hit in hits) {
            Health health = hit.transform.GetComponent<Health>();
            Rigidbody hitBody = hit.transform.GetComponent<Rigidbody>();
            if (!hitTargets.Contains(hit.transform) && health.IsDamagable()) {
                if (characterState.isLauncher) {
                    gameStateManager.RemoveLaunchTarget(hitBody);
                    StartCoroutine(LaunchNextFrame(hitBody, characterState.launchSpeed, characterState.launchTime));
                }
                health.AdjustHealth(-characterState.damage,hit.point, characterState.hitDirHash);
                hitTargets.Add(hit.transform);
                if (hitParticles) {
                    hitParticles.SetActive(true);
                    hitParticles.GetComponent<ParticleSystem>().Clear();
                    hitParticles.GetComponent<ParticleSystem>().Play();
                    hitParticles.transform.position = transform.position;
                }
                gameStateManager.BeginSlowTime(characterState.slowTimeScale, 0.015f, characterState.shakeAmount);
            }
        }
        prevPos = transform.position;
	}

    private IEnumerator LaunchNextFrame(Rigidbody hitBody, float launchSpeed, float launchTime) {
        yield return null;
        gameStateManager.AssignLaunchTarget(hitBody, launchSpeed, launchTime);
    }

    public void Reset() {
        hitTargets.Clear();
        if (hitParticles) {
            hitParticles.SetActive(false);
            hitParticles.GetComponent<ParticleSystem>().Clear();
        }
    }

}