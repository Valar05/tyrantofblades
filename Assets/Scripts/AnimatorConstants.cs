﻿using UnityEngine;

public class AnimatorConstants {
    //Params

    //Player
    public int LightAttack;
    public int HeavyAttack;
    public int Grounded;
    public int Jump;
    public int AllowCombo;
    public int Damaged;
    public int Move;
    public int Blocking;
    public int BlockInit;
    public int Parry;
    public int PlayingToCompletion;
    public int AllowMovement;
    public int SpecialAttack;
    public int HeavyAttackHold;
    public int HeavyAttackShort;
    public int CanDodge;
    public int TapStick;
    public int IsFinisher;
    public int Death;
    //Goblin
    public int StrafeRight;
    public int StrafeLeft;
    public int RunForward;
    public int Combo1;
    public int JumpLeft;
    public int JumpRight;
    public int DodgeAttackLeft;
    public int DodgeAttackRight;
    public int JumpBack;
    public int KnockUp;
    public int KnockedUpFollowthrough;
    public int GapCloser;
    public int IsCancellable;

    //Camera
    public int Shake;
    public int ShakeLittle;

    //Names
    public int LightCombo1; //Animator.StringToHash("Base Layer.LightAttacks.LightCombo1")

    public AnimatorConstants() {
        LightAttack = Animator.StringToHash("LightAttack");
        HeavyAttack = Animator.StringToHash("HeavyAttack");
        Grounded = Animator.StringToHash("Grounded");
        Jump = Animator.StringToHash("Jump");
        AllowCombo = Animator.StringToHash("AllowCombo");
        Damaged = Animator.StringToHash("Damaged");
        Move = Animator.StringToHash("Move");
        Blocking = Animator.StringToHash("Blocking");
        BlockInit = Animator.StringToHash("BlockInit");
        Parry = Animator.StringToHash("Parry");
        PlayingToCompletion = Animator.StringToHash("PlayingToCompletion");
        AllowMovement = Animator.StringToHash("AllowMovement");
        SpecialAttack = Animator.StringToHash("SpecialAttack");
        Death = Animator.StringToHash("Death");
        HeavyAttackHold = Animator.StringToHash("HeavyAttackHold");
        HeavyAttackShort = Animator.StringToHash("HeavyAttackShort");
        CanDodge = Animator.StringToHash("CanDodge");
        TapStick = Animator.StringToHash("TapStick");
        IsFinisher = Animator.StringToHash("IsFinisher");
        StrafeRight = Animator.StringToHash("StrafeRight");
        StrafeLeft = Animator.StringToHash("StrafeLeft");
        RunForward = Animator.StringToHash("RunForward");
        Combo1 = Animator.StringToHash("Combo1");
        JumpLeft = Animator.StringToHash("JumpLeft");
        JumpRight = Animator.StringToHash("JumpRight");
        DodgeAttackLeft = Animator.StringToHash("DodgeAttackLeft");
        DodgeAttackRight = Animator.StringToHash("DodgeAttackRight");
        JumpBack = Animator.StringToHash("JumpBack");
        KnockUp = Animator.StringToHash("KnockUp");
        KnockedUpFollowthrough = Animator.StringToHash("KnockedUpFollowthrough");
        GapCloser = Animator.StringToHash("GapCloser");
        IsCancellable = Animator.StringToHash("IsCancellable");
    }
}