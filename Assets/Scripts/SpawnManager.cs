﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

    public GameObject enemyPrefab;
    public bool canSpawn = true;

    Transform playerRef;
    //int enemyCount=3;
    int maxEnemies = 5;
    int enemyCount = 1;
    int waveCount;
    ArrayList activeEnemies = new ArrayList();

    void Awake() {
        if (canSpawn)
            StartCoroutine(SpawnEnemies(0.5f));
        playerRef = GameObject.Find("Player").transform;
    }

	IEnumerator SpawnEnemies(float delay) {
        yield return new WaitForSeconds(delay);
        for (int i = 0; i < enemyCount; i++) {
            GameObject enemy = GameObject.Instantiate(enemyPrefab);
            enemy.transform.position = new Vector3(Random.Range(-9, 7), -0.07f, Random.Range(-6, 8));
            enemy.GetComponent<AIUnit>().target = playerRef;
            activeEnemies.Add(enemy);
        }
        waveCount++;
    }

    public void Deregister(GameObject gameObject) {
        activeEnemies.Remove(gameObject);
        if(activeEnemies.Count <= 0) {
            if (enemyCount < maxEnemies)
                enemyCount++;
            StartCoroutine(SpawnEnemies(1f));
        }
    }
}
