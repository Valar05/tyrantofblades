﻿using UnityEngine;
using System.Collections;

public class DeathFloor : MonoBehaviour {

    void OnCollisionEnter(Collision other) {
        Health collidedHealth = other.gameObject.GetComponent<Health>();
        if (collidedHealth)
            collidedHealth.AdjustHealth(-collidedHealth.maxHealth,0);
    }
}
