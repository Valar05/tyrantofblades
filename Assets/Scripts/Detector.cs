﻿using UnityEngine;
using System.Collections;
using System;

public class Detector : MonoBehaviour {

    public string detectTag = "Enemy";
    public Transform target;

    void Update() {
        if (target && target.GetComponent<Health>().currentHealth <= 0)
            target = null;
            
    }

    public void OnTriggerEnter(Collider collider) {
        if (target == null && collider.gameObject.CompareTag(detectTag)) {
            if(collider.GetComponent<Health>().currentHealth > 0)
                target = collider.transform;
        }
    }

    public void OnTriggerExit(Collider collider) {
        if (collider.transform == target)
        {
            target = null;
        }
    }

    public void ClearIfExists(Transform transform) {
        if (transform == target)
            target = null;
    }

}
