﻿using Assets.Scripts.ActionItems;
using System;
using System.Collections.Generic;

namespace Assets.Scripts {
    public interface IActionItem {

        VariableUnitState ActionState { get; }

        int AnimParam { get; }

        bool Blocking { get; }

        bool CanExecute();

        void OnEnd();

        IActionItem Execute();
    }
}
