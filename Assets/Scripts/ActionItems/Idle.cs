﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class Idle : IActionItem {

        AIUnit aIUnit { get; set; }
        Rigidbody unitRigidbody;
        Transform unitTransform;

        public VariableUnitState ActionState {
            get { return VariableUnitState.NONE; }
        }

        public int AnimParam {
            get { return 0; }
        }

        public Idle(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            unitRigidbody = aIUnit.GetComponent<Rigidbody>();
        }

        public bool Blocking {
            get {
                return false;
            }
        }

        public bool CanExecute() {
            return true;
        }

        public IActionItem Execute() {
            unitRigidbody.velocity = new Vector3(0, 0, 0);
            return null;
        }

        public void OnEnd() { }
    }
}
