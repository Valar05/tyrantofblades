﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class CircleTarget : IActionItem {

        AIUnit aIUnit { get; set; }
        Rigidbody unitRigidbody;
        Transform unitTransform;
        int animParam;

        public VariableUnitState ActionState {
            get { return VariableUnitState.CIRCLE; }
        }

        public int AnimParam {
            get { return animParam; }
        }

        public CircleTarget(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            animParam = Animator.StringToHash("Circle");
            unitRigidbody = aIUnit.GetComponent<Rigidbody>();
            unitTransform = aIUnit.GetComponent<Transform>();
        }

        public bool Blocking {
            get {
                return aIUnit.unitState == ActionState;
            }
        }

        public bool CanExecute() {
            if (aIUnit.TargetController.circlingAttacker == aIUnit.gameObject)
                return true;
            return aIUnit.target != null && aIUnit.TargetController.circlingAttacker == null;
        }

        public IActionItem Execute() {
            aIUnit.TargetController.circlingAttacker = aIUnit.gameObject;
            Vector3 targetVector = aIUnit.target.position - aIUnit.target.position;
            unitTransform.LookAt(Vector3.Cross(targetVector,Vector3.forward));
            unitRigidbody.velocity = aIUnit.transform.forward * 3;
            return this;
        }

        public void OnEnd() {
            //unitAnimator.SetBool(AnimParam, false);
        }
    }
}
