﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class AttackTargetInRange : IActionItem {

        AIUnit aIUnit { get; set; }
        Animator unitAnimator;
        int animParam;

        public VariableUnitState ActionState {
            get { return VariableUnitState.ATTACK; }
        }

        public int AnimParam {
            get { return animParam; }
        }

        public AttackTargetInRange(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            unitAnimator = aIUnit.GetComponent<Animator>();
        }

        public bool Blocking {
            get {
                return aIUnit.unitState == ActionState;
            }
        }

        public bool CanExecute() {
            return (aIUnit.target != null && aIUnit.isAttacking && aIUnit.DistanceToTarget < 2) || unitAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AIAttack");
        }

        public IActionItem Execute() {
            if(animParam == 0) {
                int randomInt = Random.Range(0, 25);
                if (randomInt > 10)
                    animParam = Animator.StringToHash("Combo1");
                else if(randomInt > 5 )
                    animParam = Animator.StringToHash("DodgeAttackLeft");
                else
                    animParam = Animator.StringToHash("DodgeAttackRight");
            }
            aIUnit.unitState = ActionState;
            if (!unitAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AIAttack"))
                unitAnimator.SetBool(AnimParam, true);
            else {
                aIUnit.isAttacking = false;
                unitAnimator.SetBool(AnimParam, false);
            }
            return this;
        }

        public void OnEnd() {
            unitAnimator.SetBool(AnimParam, false);
            animParam = 0;
            aIUnit.CanFaceTarget();
			aIUnit.isAttacking = false;
			aIUnit.target.GetComponent<PlayerController> ().RemoveFromAttackerList (aIUnit.gameObject);
            aIUnit.ResetAttack();
        }
    }
}
