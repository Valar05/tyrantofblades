﻿using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts
{
    [System.Serializable]
    public class StrafeTarget : IActionItem
	{
		AIUnit aIUnit { get; set; }
		Animator unitAnimator;
		int animParam;

		public VariableUnitState ActionState {
			get { return VariableUnitState.STRAFE; }
		}

		public StrafeTarget(AIUnit aIUnit) {
			this.aIUnit = aIUnit;
			unitAnimator = aIUnit.GetComponent<Animator>();
		}

		public int AnimParam {
			get { return animParam; }
		}

		public bool Blocking {
			get {
				return aIUnit.unitState == ActionState;
			}
		}

		public bool CanExecute() {
			return aIUnit.target != null;
		}

        public IActionItem Execute() {
            if (animParam == 0) {
                if (Random.Range(1, 3) > 1)
                    animParam = Animator.StringToHash("StrafeLeft");
                else
                    animParam = Animator.StringToHash("StrafeRight");
            }
            unitAnimator.SetBool(AnimParam,true);
			return this;
		}

		public void OnEnd() {
			unitAnimator.SetBool(AnimParam, false);
            animParam = 0;
		}
	}
}

