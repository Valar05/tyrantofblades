﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.ActionItems {
    [System.Serializable]
    public enum VariableUnitState {
        ATTACK,APPROACH_PLAYER,STRAFE,CIRCLE,IDLE,NONE,DODGE,DODGEBACK,
        DAMAGED
    }
}
