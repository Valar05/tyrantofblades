﻿using UnityEngine;

namespace Assets.Scripts.ActionItems {
    [System.Serializable]
    class AcceptDamage : IActionItem {

        AIUnit aIUnit { get; set; }

        public AcceptDamage(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
        }

        public VariableUnitState ActionState {
            get {
                return VariableUnitState.DAMAGED;
            }
        }

        public int AnimParam {
            get {
                return 0;
            }
        }

        public bool Blocking {
            get {
                return aIUnit.unitState == ActionState;
            }
        }

        public bool CanExecute() {
            return aIUnit.characterStateManager.state.damaged;
        }

        public IActionItem Execute() {
            return this;
        }

        public void OnEnd() {
            
        }
    }
}
