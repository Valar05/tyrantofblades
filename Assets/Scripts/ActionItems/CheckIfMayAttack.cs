﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class CheckIfMayAttack : IActionItem {

        AIUnit aIUnit { get; set; }

        public VariableUnitState ActionState {
            get { return VariableUnitState.ATTACK; }
        }

        public CheckIfMayAttack(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
        }

        public int AnimParam {
            get { return 0; }
        }

        public bool Blocking {
            get {
                return false;
            }
        }

        public bool CanExecute() {
            return aIUnit.target != null;
        }

        public IActionItem Execute() {
            if (!aIUnit.isAttacking) {
                if (Random.Range(0, 1000) <= 13) {
                    aIUnit.isAttacking = aIUnit.TargetController.AddToAttackerList (aIUnit.gameObject);
					if (aIUnit.isAttacking && aIUnit.target == aIUnit.TargetController.circlingAttacker)
						aIUnit.TargetController.circlingAttacker = null;
				}
            }
            return null;
        }

        public void OnEnd() { }

     }
}
