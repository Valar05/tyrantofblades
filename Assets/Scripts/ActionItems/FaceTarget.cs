﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.ActionItems {
    class FaceTarget : IActionItem {
        Transform unitTransform;
        AIUnit aIUnit;
        Quaternion targetRotation;

        public FaceTarget(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            unitTransform = aIUnit.transform;
        }

        public VariableUnitState ActionState { get { return VariableUnitState.NONE; } }

        public int AnimParam { get { return 0; } }

        public bool Blocking {  get { return false; } }

        public bool CanExecute() {
            return aIUnit.target != null && aIUnit.FaceTargetAllowed;
        }

        public IActionItem Execute() {
            Vector3 fullForwardRotation = aIUnit.target.position - unitTransform.position;
            Vector3 fixedForwardRotation = new Vector3(fullForwardRotation.x, 0, fullForwardRotation.z);
            targetRotation = Quaternion.LookRotation(fixedForwardRotation);
            unitTransform.rotation = Quaternion.RotateTowards(unitTransform.rotation, targetRotation, 450 * Time.deltaTime);
            return null;
        }

        public void OnEnd() { }
    }
}
