﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class DodgeBackward : IActionItem {

        AIUnit aIUnit { get; set; }
        Animator unitAnimator;
        int animParam;

        public VariableUnitState ActionState {
            get { return VariableUnitState.DODGEBACK; }
        }

        public int AnimParam {
            get { return animParam; }
        }

        public DodgeBackward(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            unitAnimator = aIUnit.GetComponent<Animator>();
            animParam = Animator.StringToHash("JumpBack");
        }

        public bool Blocking {
            get {
                return aIUnit.unitState == ActionState;
            }
        }

        public bool CanExecute() {
            int chanceToDodge = Random.Range(0, 1000);
            if(chanceToDodge < aIUnit.TimeNearTarget * 8)
                return (aIUnit.target != null && !aIUnit.isAttacking && aIUnit.DistanceToTarget < 2) || unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("DodgeBackward");
            return false;
        }

        public IActionItem Execute() {
            aIUnit.unitState = ActionState;
            if (!unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("DodgeBackward"))
                unitAnimator.SetBool(AnimParam, true);
            else {
                unitAnimator.SetBool(AnimParam, false);
            }
            return this;
        }

        public void OnEnd() {
            unitAnimator.SetBool(AnimParam, false);
        }
    }
}
