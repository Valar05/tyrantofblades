﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class CloseGapInRange : IActionItem {

        AIUnit aIUnit { get; set; }
        Animator unitAnimator;
        int animParam;

        public VariableUnitState ActionState {
            get { return VariableUnitState.ATTACK; }
        }

        public int AnimParam {
            get { return animParam; }
        }

        public CloseGapInRange(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            unitAnimator = aIUnit.GetComponent<Animator>();
            animParam = Animator.StringToHash("GapCloser");
        }

        public bool Blocking {
            get {
                return aIUnit.unitState == ActionState;
            }
        }

        public bool CanExecute() {
            int chanceToExecute = 0;
            if (aIUnit.prevAction is DodgeBackward)
                chanceToExecute = 100;
            else
                chanceToExecute = Random.Range(0, 100);
            return (aIUnit.target != null && chanceToExecute > 90 && aIUnit.isAttacking && (aIUnit.DistanceToTarget > 2.5 && aIUnit.DistanceToTarget < 3)) || unitAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AIAttack");
        }

        public IActionItem Execute() {
            aIUnit.unitState = ActionState;
            if (!unitAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AIAttack"))
                unitAnimator.SetBool(AnimParam, true);
            else {
                aIUnit.isAttacking = false;
                unitAnimator.SetBool(AnimParam, false);
            }
            return this;
        }

        public void OnEnd() {
            unitAnimator.SetBool(AnimParam, false);
            aIUnit.CanFaceTarget();
			aIUnit.isAttacking = false;
			aIUnit.target.GetComponent<PlayerController> ().RemoveFromAttackerList (aIUnit.gameObject);
            aIUnit.ResetAttack();
        }
    }
}
