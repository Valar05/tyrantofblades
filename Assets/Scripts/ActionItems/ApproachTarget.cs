﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.ActionItems;

namespace Assets.Scripts {
    [System.Serializable]
    class ApproachTarget : IActionItem {

        AIUnit aIUnit { get; set; }
        Animator unitAnimator;
        int animParam;

        public VariableUnitState ActionState {
            get { return VariableUnitState.APPROACH_PLAYER; }
        }

        public ApproachTarget(AIUnit aIUnit) {
            this.aIUnit = aIUnit;
            animParam = Animator.StringToHash("RunForward");
            unitAnimator = aIUnit.GetComponent<Animator>();
        }

        public int AnimParam {
            get { return animParam; }
        }

        public bool Blocking {
            get {
                return aIUnit.unitState == ActionState;
            }
        }

        public bool CanExecute() {
				return aIUnit.isAttacking && aIUnit.target != null && aIUnit.DistanceToTarget > 2;
        }

        public IActionItem Execute() {
            unitAnimator.SetBool(AnimParam,true);
            return this;
        }

        public void OnEnd() {
            unitAnimator.SetBool(AnimParam, false);
        }
    }
}
