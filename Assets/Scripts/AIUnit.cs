﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.ActionItems;
using Xft;
using Assets.Scripts.States;

public class AIUnit : MonoBehaviour {

    public List<IActionItem> actionList = new List<IActionItem>();
    public Transform target;
    public bool isAttacking;
    public VariableUnitState unitState;
    public bool finishable;
    IActionItem currentAction;
    public IActionItem prevAction;
    public CharacterStateManager characterStateManager;
    Animator myAnimator;
    Rigidbody myRigidbody;
    bool faceTargetAllowed = true;
    float timeNearTarget;
    public GameObject weaponHitbox;
    public LayerMask groundLayer;
    Transform groundCheck;
    bool grounded = true;
    XWeaponTrail weaponTrail;
    Detector enemyDetectorNear;
    AnimatorConstants aConstants;
    PlayerController targetController;
    public bool counterable;

    public float DistanceToTarget {
        get {
            return target == null ? -1 : Vector3.Distance(transform.position, target.position);
        }
    }

    public PlayerController TargetController {
        get {
            if (target != null)
                targetController = target.GetComponent<PlayerController>();
            return targetController;
        }
    }

	void Awake () {
        characterStateManager = GetComponent<CharacterStateManager>();
        groundCheck = transform.Find("GroundCheck");
        aConstants = GameObject.Find("GameStateManager").GetComponent<GameStateManager>().animConstants;
        enemyDetectorNear = GameUtils.FindInChild(transform.GetComponentsInChildren<Transform>(), "EnemyDetectorNear").GetComponent<Detector>();
        weaponHitbox = transform.GetComponentInChildren<MeleeHitbox>().gameObject;
        weaponHitbox.SetActive(false);
        weaponTrail = GetComponentInChildren<XWeaponTrail>();
        weaponTrail.Deactivate();
        myAnimator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody>();
        unitState = VariableUnitState.NONE;

        actionList.Add(new AcceptDamage(this));

        actionList.Add(new FaceTarget(this));

		actionList.Add(new AttackTargetInRange(this));

        actionList.Add(new CloseGapInRange(this));

		actionList.Add(new ApproachTarget(this));

		actionList.Add(new CheckIfMayAttack(this));

        actionList.Add(new DodgeBackward(this));

		actionList.Add (new StrafeTarget (this));

        //actionList.Add(new CircleTarget(this));

        actionList.Add(new Idle(this));

	}
	

	void FixedUpdate () {
        CheckGround();
        CalculateTimeNearPlayer();
        foreach (IActionItem actionItem in actionList) {
            EvaluateAction(actionItem);
            if (actionItem.Blocking) {
                break;
            }
        }
	}

    void EvaluateAction(IActionItem actionItem) {
        if (actionItem.CanExecute()) {
            unitState = actionItem.ActionState;
            IActionItem returnItem = actionItem.Execute();
            if(returnItem != null && currentAction != returnItem) {
                if(currentAction!= null)
                    currentAction.OnEnd();
                prevAction = currentAction;
                currentAction = returnItem;
            }
        }
        else {
            if (actionItem == currentAction) {
                currentAction.OnEnd();
                prevAction = currentAction;
                currentAction = null;
            }
            if (unitState == actionItem.ActionState)
                unitState = VariableUnitState.NONE;
        }
    }

    void CalculateTimeNearPlayer() {
        if (DistanceToTarget > -1 && DistanceToTarget <= 2)
            timeNearTarget += Time.deltaTime;
        else
            timeNearTarget = 0f;
    }

    public float TimeNearTarget {
        get { return timeNearTarget; }
    }

    public void OnAnimatorMove() {
        CharacterState myState = characterStateManager.state;
        if (myState.isRootMotion) {
            if (!enemyDetectorNear.target || !myState.isAttack)
                myAnimator.ApplyBuiltinRootMotion();
            else
                myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);
        }
    }

    public bool FaceTargetAllowed {
        get { return faceTargetAllowed; }
    }

    public void CanFaceTarget() {
        faceTargetAllowed = true;
        myRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }
    public void CannotFaceTarget() {
        faceTargetAllowed = false;
        myRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
    }

    public void ResetAttack() {
        weaponHitbox.SetActive(false);
        weaponHitbox.GetComponent<MeleeHitbox>().Reset();
    }

    public void IsCounterable() {
        counterable = true;
    }

    public void IsNotCounterable() {
        counterable = false;
    }

    public void BeginActiveFrame() {
        weaponHitbox.SetActive(true);
    }


    public void Interrupt() {
        if(currentAction != null)
            currentAction.OnEnd();
        currentAction = null;
        IsNotCounterable();
        unitState = VariableUnitState.NONE;
        weaponHitbox.SetActive(false);
        if (targetController) {
            targetController.RemoveFromAttackerList(gameObject);
        }
    }

    void CheckGround() {
        float radius = .45f;
        Vector3 pos = groundCheck.position + Vector3.up * (radius * 0.9f);
        grounded = Physics.CheckSphere(pos, radius, groundLayer);
        myAnimator.SetBool(aConstants.Grounded, grounded);
        if (myRigidbody.velocity.y < 0 && grounded)
            myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, 0, myRigidbody.velocity.z);
    }

    public void BeginWeaponTrail(float trailTime) {
        weaponTrail.Activate();
        StartCoroutine(DeactivateWeaponTrail(trailTime));
    }

    public void ResetDamage() {
        myAnimator.ResetTrigger(aConstants.Damaged);
        myAnimator.ResetTrigger(aConstants.KnockUp);
    }

    IEnumerator DeactivateWeaponTrail(float trailTime) {
        yield return new WaitForSeconds(trailTime);
        weaponTrail.StopSmoothly(.34f);
    }

    public void AssumeKnockupPosition() {
        if(target)
            transform.position = target.transform.Find("KnockupPosition").position;
        myAnimator.SetTrigger(aConstants.KnockedUpFollowthrough);
    }

    public void IntensifyFall() {
        myRigidbody.AddForce(0, -1500, 0);
    }
}
