﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {

    public int maxAttackers;
    public GameObject gameOverCanvas;
    public List<Rigidbody> currentlyLaunching = new List<Rigidbody>();
    public AnimatorConstants animConstants;
    Camera cam;
    int currentScene;
    Canvas gameOverScreen;
    public AnimationCurve launchCurve;
    public GameObject player;

    void Awake() {
        Time.timeScale = 1f;
        animConstants = new AnimatorConstants();
        cam = Camera.main;
        currentScene = SceneManager.GetActiveScene().buildIndex;
        gameOverScreen = gameOverCanvas.GetComponent<Canvas>();
        gameOverCanvas.SetActive(false);
        Physics.IgnoreLayerCollision(8, 11); //Player - EnemyDead
        Physics.IgnoreLayerCollision(10, 11); //Enemy - EnemyDead
        Physics.IgnoreLayerCollision(8, 10); //Player - Enemy
        player = GameObject.Find("Player");
    }

    public void BeginSlowTime(float slowedBy, float slowedFor, float shakeAmount) {
        StartCoroutine(SlowTime(slowedBy, slowedFor, shakeAmount));
    }

    IEnumerator SlowTime(float slowedBy, float slowedFor, float shakeAmount) {
        yield return new WaitForEndOfFrame();
        Time.timeScale = slowedBy;
        yield return new WaitForSeconds(slowedFor);
        Time.timeScale = 1f;
        StartCoroutine(ShakeCamera(shakeAmount));
    }

    public void LaunchTargets(float launchTime, float launchSpeed, List<Transform> hitTargets) {
        StartCoroutine(LaunchTargetsRoutine(launchTime, launchSpeed, launchCurve, hitTargets));
    }

    IEnumerator LaunchTargetsRoutine(float launchTime, float launchSpeed, AnimationCurve launchCurve, List<Transform> hitTargets) {
        for (float f = 0; f < launchTime; f += Time.deltaTime) {
            foreach (Transform target in hitTargets) {
                Rigidbody targetRigidbody = target.GetComponent<Rigidbody>();
                if (targetRigidbody) {
                    targetRigidbody.velocity = new Vector3(targetRigidbody.velocity.x, launchSpeed * launchCurve.Evaluate(f), targetRigidbody.velocity.z);
                }
            }
            yield return null;
        }
        foreach (Transform target in hitTargets) {
            Rigidbody targetRigidbody = target.GetComponent<Rigidbody>();
            if (targetRigidbody) {
                targetRigidbody.velocity = Vector3.zero;
            }
        }
    }

    public void GameOver() {
        StartCoroutine(DoGameOver());
    }

    IEnumerator DoGameOver() {
        yield return new WaitForSeconds(.1f);
        StartCoroutine(SlowTime(.3f, .15f, 0f));
        StartCoroutine(FadeGameOverScreen());
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(currentScene);
    }

    IEnumerator FadeGameOverScreen() {
        gameOverCanvas.SetActive(true);
        CanvasRenderer rend = gameOverScreen.GetComponentInChildren<CanvasRenderer>();
        Text text = gameOverScreen.GetComponentInChildren<Text>();
        for (float f=0;f<=.3f; f += Time.deltaTime) {
            rend.SetAlpha(f*3);
            text.color = new Color(text.color.r, text.color.g, text.color.b, Mathf.Clamp(text.color.a + f * 3,0,255));
            yield return null;
        }
        rend.SetAlpha(1f);
    }

    public void AssignLaunchTarget(Rigidbody launchTarget, float launchSpeed, float launchTime) {
        if (!currentlyLaunching.Contains(launchTarget)) {
            currentlyLaunching.Add(launchTarget);
            StartCoroutine(LaunchTargets(launchTarget, launchSpeed, launchTime));
        }
    }

    public void IgnoreCollisionWithLaunchedTargets(Collider collider) {
        StartCoroutine(IgnoreCollisionForTime(collider));     
    }

    IEnumerator IgnoreCollisionForTime(Collider collider) {
        foreach (Rigidbody rigidbody in currentlyLaunching) {
            Physics.IgnoreCollision(collider, rigidbody.GetComponent<Collider>());
        }
        yield return new WaitForSeconds(0.14f);
        foreach (Rigidbody rigidbody in currentlyLaunching) {
            Physics.IgnoreCollision(collider, rigidbody.GetComponent<Collider>(), false);
        }
    }

    IEnumerator LaunchTargets(Rigidbody launchTarget, float launchSpeed, float launchTime) {
        
        for (float f = 0; f < launchTime; f += Time.deltaTime) {
            if (!currentlyLaunching.Contains(launchTarget)) {
                break;
            }
            float z = 0f;
            if (launchTarget.gameObject!= player) {
                Vector3 playerPos = player.transform.position;
                Vector3 targetPos = launchTarget.transform.position;

                if (Vector3.Distance(new Vector3(playerPos.x,0,playerPos.z), new Vector3(targetPos.x,0,targetPos.z)) > 1.5)
                    z = -1f;
                if (Vector3.Distance(new Vector3(playerPos.x, 0, playerPos.z), new Vector3(targetPos.x, 0, targetPos.z)) < .8)
                    z = 1f;
                //launchTarget.transform.LookAt(new Vector3(playerPos.x, targetPos.y, playerPos.z));
            }
            launchTarget.velocity = new Vector3(0, launchSpeed * launchCurve.Evaluate(f), z);
            yield return null;
        }
        currentlyLaunching.Remove(launchTarget);
            //rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0, rigidBody.velocity.z);
    }

    IEnumerator ShakeCamera(float shakeAmount) {
        float shakeProgress = shakeAmount;
        Transform camTransform = cam.transform;
        while (Mathf.Abs(shakeProgress ) > .1){
            camTransform.eulerAngles = new Vector3(camTransform.eulerAngles.x, camTransform.eulerAngles.y, shakeProgress);
            shakeProgress = shakeProgress / 2 * -1;
            yield return null;
        }
        camTransform.eulerAngles = new Vector3(camTransform.eulerAngles.x, camTransform.eulerAngles.y, 0f);
    }

    internal void RemoveLaunchTarget(Rigidbody theRigidbody) {
        currentlyLaunching.Remove(theRigidbody);
    }
}
