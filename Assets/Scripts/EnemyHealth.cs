﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : Health {

    Animator myAnimator;
    PlayerController player;
    Rigidbody myRigidbody;
    Rigidbody[] ragdollParts;
    Vector3 damageVec;
    float damageForce;
    AIUnit myUnit;

    void Awake() {
        currentHealth = maxHealth;
        myAnimator = GetComponent<Animator>();
        myUnit = GetComponent<AIUnit>();
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        damageVec = Vector3.zero;
        myRigidbody = GetComponent<Rigidbody>();
        ragdollParts = GetComponentsInChildren<Rigidbody>();
        foreach( Rigidbody  part in ragdollParts) {
            if (part.gameObject != gameObject) {
                part.isKinematic = true;
                part.GetComponent<Collider>().enabled = false;
            }
        }
    }

    public override void AdjustHealth(int healthAdjustment, Vector3 damageVec, int hitStateHash) {
        this.damageVec = damageVec;
        myAnimator.SetTrigger(hitStateHash);
        AdjustHealth(healthAdjustment, hitStateHash);
    }


    public override void AdjustHealth(int healthAdjustment, int hitStateHash) {
        currentHealth += healthAdjustment;
        player.RemoveFromAttackerList(this.gameObject);
        transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
        //if (damageForce > 0)
        //    myRigidbody.AddForce(-transform.forward * damageForce);
        if (currentHealth <= 0 && !dead)
            Die();
        else
            myAnimator.SetTrigger(hitStateHash);
        myUnit.Interrupt();


    }

    protected override void Die() {
        dead = true;
        gameObject.GetComponent<AIUnit>().enabled = false;
        foreach (Rigidbody part in ragdollParts) {
            part.isKinematic = false;
            part.GetComponent<Collider>().enabled = true;
            part.AddExplosionForce(500f, damageVec, 6f);
            part.gameObject.layer = 11;
        }
        StartCoroutine(EndRagdoll());
        //GetComponent<Animator>().SetTrigger(Animator.StringToHash("Death"));
        GetComponent<Animator>().enabled = false;
        myRigidbody.isKinematic = true;
        GetComponent<Collider>().enabled = false;
        player.GetComponentInChildren<Detector>().ClearIfExists(transform);
        gameObject.GetComponent<AIUnit>().ResetAttack();
        GameObject.Find("GameStateManager").GetComponent<SpawnManager>().Deregister(gameObject);

    }

    IEnumerator EndRagdoll() {
        yield return new WaitForSeconds(5f);
        foreach (Rigidbody part in ragdollParts) {
            part.isKinematic = true;
            part.GetComponent<Collider>().enabled = false;
        }
    }

    public override bool IsDamagable() {
        return !dead;
    }
}
