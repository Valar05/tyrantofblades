﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PlayerHealth : Health {

    public Image healthBar;
    private Animator playerAnimator;
    public bool invulnerable;

    GameStateManager gameStateManager;
    bool damageInvul;

	void Awake () {
        currentHealth = maxHealth;
        playerAnimator = GetComponent<Animator>();
        playerAnimator = GetComponent<Animator>();
        gameStateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
	}

    public override void AdjustHealth(int healthAdjustment, Vector3 point, int hitStateHash) {
        AdjustHealth(healthAdjustment, hitStateHash);
    }

    public override void AdjustHealth(int healthAdjustment, int hitStateHash) {
        if (invulnerable || damageInvul || dead)
            return;
        currentHealth += healthAdjustment;
        if (healthAdjustment < 0) {
            playerAnimator.SetTrigger(hitStateHash);
            StartCoroutine(DamageInvulnerability());
            healthBar.fillAmount = (float)currentHealth / (float)maxHealth;
            if (currentHealth <= 0)
                Die();
        }
    }

    protected override void Die() {
        dead = true;
        playerAnimator.SetTrigger(gameStateManager.animConstants.Death);
        gameStateManager.GameOver();
    }

    IEnumerator DamageInvulnerability() {
        damageInvul = true;
        yield return new WaitForSeconds(.5f);
        damageInvul = false;
    }

    public override bool IsDamagable() {
        return !(invulnerable || damageInvul);
    }
}
