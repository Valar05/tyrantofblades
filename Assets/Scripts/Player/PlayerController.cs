﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using Xft;
using System.Collections.Generic;
using Assets.Scripts.States;
using System;

public class PlayerController : MonoBehaviour {
    public int moveSpeed;
    public float jumpSpeed;
    public bool allowMovement = true;
    public AnimationCurve jumpCurve;
    public LayerMask groundLayer;
    public GameObject circlingAttacker;
    public ParticleSystem multihitParticles;

    Rigidbody myRigidbody;
    Animator myAnimator;
    Camera cam;
    bool moveValue;
    bool isJumping;
    int jumpCharges;
    bool prevBlock;
    bool canDodge;
    bool grounded = true;
    bool iFrameActive;
    Transform groundCheck;
    GameObject weaponHitbox;
    public List<GameObject> attackerList = new List<GameObject>(2);
    GameStateManager gameStateManager;
    AnimatorConstants aConstants;
    Detector enemyDetector;
    Detector enemyDetectorNear;
    XWeaponTrail weaponTrail;
    PlayerHealth myHealth;
    CharacterStateManager characterStateManager;
    CharacterState myState;
    float timeSinceNeutral;

    void Awake () {
        myAnimator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody>();
        cam = Camera.main;
        groundCheck = transform.Find("GroundCheck");
        myHealth = GetComponent<PlayerHealth>();
        characterStateManager = GetComponent<CharacterStateManager>();
        //TODO: Fix this with real hitbox
        //weaponHitbox = GameUtils.FindInChild(transform.GetComponentsInChildren<Transform>(),"Hitbox");
        weaponHitbox = transform.GetComponentInChildren<MeleeHitbox>().gameObject;
        weaponHitbox.SetActive(false);
        enemyDetector = GameUtils.FindInChild(transform.GetComponentsInChildren<Transform>(), "EnemyDetector").GetComponent<Detector>();
        enemyDetectorNear = GameUtils.FindInChild(transform.GetComponentsInChildren<Transform>(), "EnemyDetectorNear").GetComponent<Detector>();
        gameStateManager = GameObject.Find("GameStateManager").GetComponent<GameStateManager>();
        aConstants = gameStateManager.animConstants;
        weaponTrail = GetComponentInChildren<XWeaponTrail>();
        EndWeaponTrail();
        multihitParticles.Stop();
    }
	
	void Update () {
        myState = characterStateManager.state;
        myAnimator.SetBool(aConstants.IsCancellable, myState.isCancellable);
        if (!myHealth.dead) {
            CheckGround();
            DetectBlock();
            DetectJump();
            Move();
            DetectAttack();
        }
	}

    private void DetectBlock() {
        bool blocking = Input.GetButton("Block") || Input.GetAxis("Block") > 0;
        myAnimator.SetBool(aConstants.Blocking,blocking );
        if (blocking)
            multihitParticles.Stop();
        if (!myState.isBlocking) {
            if (blocking && !prevBlock) {
                myAnimator.SetTrigger(aConstants.BlockInit);
                ParryCheck();
            }
            if(!iFrameActive)
                myHealth.invulnerable = myState.invulLevel > 0;
        }
        prevBlock = blocking;
    }

    void ParryCheck() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2.6f, weaponHitbox.GetComponent<MeleeHitbox>().collisionLayers);
        foreach (Collider hit in hitColliders) {
            AIUnit enemy = hit.GetComponent<AIUnit>();
            if (enemy && enemy.counterable) {
                transform.LookAt(new Vector3(hit.transform.position.x, transform.position.y, hit.transform.position.z));
                if(grounded)
                    myAnimator.SetTrigger(aConstants.Parry);
                StartCoroutine(SlowTime());
            }
        }
    }

    void FinisherCheck() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2.6f, weaponHitbox.GetComponent<MeleeHitbox>().collisionLayers);
        foreach (Collider hit in hitColliders) {
            AIUnit enemy = hit.GetComponent<AIUnit>();
            if (enemy && enemy.finishable) {
                transform.LookAt(new Vector3(hit.transform.position.x, transform.position.y, hit.transform.position.z));
                myAnimator.SetBool(aConstants.IsFinisher,true);
            }
        }
    }

    IEnumerator SlowTime() {
        for (float i = 0; i <= .025; i += Time.deltaTime) {
            Time.timeScale = Mathf.Clamp(i*2,0.2f,1);
            yield return null;
        }
        Time.timeScale = 1f;
    }

    void Move() {

        if (!myState.actionEnabled)
            DisallowMovement();

        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        if(input != Vector3.zero && (myState.actionEnabled || myAnimator.GetBool(aConstants.AllowCombo) || myAnimator.GetBool(aConstants.Blocking)))
                transform.rotation = Quaternion.LookRotation(input);

        input = cam.transform.TransformDirection(input);
        //TODO: Fix this
        //if(!grounded)
            //myRigidbody.velocity = new Vector3(moveSpeed * input.x, myRigidbody.velocity.y, -moveSpeed * input.y * 2.5f);

        myAnimator.SetFloat("Move", (Mathf.Abs(input.x) + Mathf.Abs(input.z)));
        timeSinceNeutral += Time.deltaTime;
        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) + Mathf.Abs(Input.GetAxisRaw("Vertical")) == 0f)
            timeSinceNeutral = 0f;
        
        if (timeSinceNeutral <= 0.2f && (Mathf.Abs(Input.GetAxisRaw("Horizontal")) + Mathf.Abs(Input.GetAxisRaw("Vertical")) >= .8f))
            myAnimator.SetBool(aConstants.TapStick, true);
        else
            myAnimator.SetBool(aConstants.TapStick, false);
        if (!canDodge)
            canDodge = true;
        myAnimator.SetBool(aConstants.CanDodge, canDodge);
    }

    private IEnumerator CountdownToLaunch() {
        yield return new WaitForSeconds(.08f);
        gameStateManager.AssignLaunchTarget(myRigidbody, characterStateManager.state.launchSpeed * .7f, characterStateManager.state.launchTime);
    }

    void DetectJump() {
        if (Input.GetButton("Jump")) {
            if (Input.GetButtonDown("Jump") && !isJumping && jumpCharges > 0) {
                jumpCharges--;
                isJumping = true;
                if (!myState.isCancellable) {
                    myAnimator.SetBool(aConstants.PlayingToCompletion, false);
                    StartCoroutine(Jump());
                }
                else
                    myAnimator.SetBool(aConstants.PlayingToCompletion, true);
                multihitParticles.Stop();
            }
        }
        else
            isJumping = false;
        myAnimator.SetBool(aConstants.Jump, isJumping);
    }

    void DetectAttack() {
        if (Input.GetButtonDown("LightAttack")) {
            if(enemyDetector.target)
                transform.LookAt(new Vector3(enemyDetector.target.position.x, transform.position.y, enemyDetector.target.position.z));
            myAnimator.SetTrigger(aConstants.LightAttack);
            myRigidbody.velocity = new Vector3(0, 0, 0);
            myAnimator.SetBool("PlayingToCompletion", true);
        }
        if (Input.GetButtonDown("HeavyAttack")) {
            if (enemyDetector.target)
                transform.LookAt(new Vector3(enemyDetector.target.position.x, transform.position.y, enemyDetector.target.position.z));
            myAnimator.SetFloat(aConstants.Move, 0f);
            myRigidbody.velocity = new Vector3(0, 0, 0);
            myAnimator.SetBool(aConstants.PlayingToCompletion, true);
            StartCoroutine(DetectHeavyLongPress());
            if (grounded)
                FinisherCheck();
        }
        if (Input.GetButtonDown("SpecialAttack")) {
            if (enemyDetector.target)
                transform.LookAt(new Vector3(enemyDetector.target.position.x, transform.position.y, enemyDetector.target.position.z));
            myAnimator.SetTrigger(aConstants.SpecialAttack);
            myRigidbody.velocity = new Vector3(0, 0, 0);
            myAnimator.SetBool(aConstants.PlayingToCompletion, true);
        }
    }

    IEnumerator DetectHeavyLongPress() {
        float timeHeld = 0;
        while (1 == 1) {
            if (Input.GetButtonUp("HeavyAttack") || timeHeld > .22f)
                break;
            timeHeld += Time.deltaTime;
            yield return null;
        }
        if (timeHeld > .22f)
            myAnimator.SetTrigger(aConstants.HeavyAttackHold);
        else
            myAnimator.SetTrigger(aConstants.HeavyAttack);
    }

    IEnumerator Jump() {
        if(myAnimator.GetFloat(aConstants.Move) >= 0.7f)
            myRigidbody.AddForce(400 * transform.forward);
        for (float f = 0; f < 0.25f; f += Time.deltaTime) {
            if (!isJumping) {
                myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, 0, myRigidbody.velocity.z);
                yield break;
            }
            myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, jumpSpeed * jumpCurve.Evaluate(f), myRigidbody.velocity.z);
            yield return null;
        }
        isJumping = false;
        myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, 0, myRigidbody.velocity.z);
    }

    void CheckGround() {
        float radius = .45f;
        Vector3 pos = groundCheck.position + Vector3.up * (radius * 0.9f);
        grounded = Physics.CheckSphere(pos, radius, groundLayer);
        if(grounded)
            jumpCharges = 2;
        myAnimator.SetBool(aConstants.Grounded,grounded);
    }

    public bool AddToAttackerList(GameObject attacker) {
        if (attackerList.Count < gameStateManager.maxAttackers) {
            attackerList.Add(attacker);
            return true;
        }
        return false;
    }

    public void RemoveFromAttackerList(GameObject attacker) {
        attackerList.Remove(attacker);
    }

    public void EndAttack() {
        weaponHitbox.SetActive(false);
        myAnimator.SetBool(aConstants.AllowCombo,false);
    }

    public void ResetAttack() {
        weaponHitbox.GetComponent<MeleeHitbox>().Reset();
    }

    public void BeginActiveFrame() {
        weaponHitbox.SetActive(true);
        ResetAttack();
    }

    public void EndActiveFrame() {
        weaponHitbox.SetActive(false);
        weaponTrail.StopSmoothly(.3f);
    }

    public void AllowCombo() {
        myAnimator.SetBool(aConstants.AllowCombo, true);
    }


    public void OnAnimatorMove() {
        if (myState.isRootMotion) {
            if (!enemyDetectorNear.target)
                myAnimator.ApplyBuiltinRootMotion();
            else
                myRigidbody.velocity = new Vector3(0f, myRigidbody.velocity.y, 0f);
        }
        if (myState.isFallFaster) {
            myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, -30f, myRigidbody.velocity.z);
        }
    }

    public void BeginWeaponTrail (float time) {
        weaponTrail.Activate();
        StartCoroutine(DisableWeaponTrail(time));
    }

    public void EndWeaponTrail() {
        weaponTrail.Deactivate();
    }

    public void AllowMovement() {
        allowMovement = true;
        myAnimator.SetBool(aConstants.AllowMovement, true);
    }

    public void DisallowMovement() {
        allowMovement = false;
        myAnimator.SetBool(aConstants.AllowMovement, false);
    }

    public void DidDodge() {
        canDodge = false;
    }

    public void PlayMultiHitParticles() {
        multihitParticles.Play();
    }

    public void StartInvul(float time) {

        StartCoroutine(InvulnerableForTime(time));
    }

    public void ResetLaunch() {
        gameStateManager.RemoveLaunchTarget(myRigidbody);
    }

    IEnumerator DisableWeaponTrail(float time) {
        yield return new WaitForSeconds(time);
        weaponTrail.StopSmoothly(.3f);
    }

    IEnumerator InvulnerableForTime(float time) {
        iFrameActive = true;
        myHealth.invulnerable = true;
        yield return new WaitForSeconds(time);
        myHealth.invulnerable = false;
        iFrameActive = false;
    }

}
