﻿using UnityEngine;
using System.Collections;

public class AllowMovementOnExit : StateMachineBehaviour {

    public override void OnStateMachineExit(Animator animator, int stateMachinePathHash) {
        animator.gameObject.GetComponent<PlayerController>().EndAttack();
        animator.ResetTrigger("LightAttack");
    }

}
