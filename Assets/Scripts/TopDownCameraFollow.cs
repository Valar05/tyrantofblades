﻿using UnityEngine;
using System.Collections;

public class TopDownCameraFollow : MonoBehaviour {
    public Transform target;

    void LateUpdate() {
        
        Vector3 targetPos = new Vector3(target.position.x, target.position.y + 6f, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPos, 2f * Time.deltaTime);
    }
}