﻿using UnityEngine;
using System.Collections;

public class SetIsFinishable : StateMachineBehaviour {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.GetComponent<AIUnit>().finishable = true;
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.GetComponent<AIUnit>().finishable = false;
    }

}
