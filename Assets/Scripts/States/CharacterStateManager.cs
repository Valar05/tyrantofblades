﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;
using System.IO;
using System;
using Assets.Scripts.States;

public class CharacterStateManager : MonoBehaviour {

    public string fileName = "PlayerStateJSON";
    public CharacterState state;

    Dictionary<int, CharacterState> states = new Dictionary<int, CharacterState>();
    Animator myAnimator;

    void Start() {
        Load();
        myAnimator = gameObject.GetComponent<Animator>();
    }

    private void Load() {
        string jsonText = (Resources.Load(fileName) as TextAsset).text;
        JSONArray json = JSONNode.Parse(jsonText).AsArray;
        for (int i = 0; i < json.Count; i++) {
            CharacterState newState = new CharacterState(json[i]);
            states.Add(Animator.StringToHash(newState.name), newState);
        }
    }
    
    void Update() {
        state = states[myAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash];
    }
    
}