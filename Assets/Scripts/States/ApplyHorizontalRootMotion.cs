﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

public class ApplyHorizontalRootMotion : StateMachineBehaviour {

    Detector closeDetector;
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Rigidbody parentRigidBody = animator.GetComponentInParent<Rigidbody>();
        if (!closeDetector) {
            closeDetector = GameUtils.FindInChild(animator.transform.GetComponentsInChildren<Transform>(), "EnemyDetectorNear").GetComponent<Detector>();
        }
        if (!closeDetector.target && animator.GetBool("Grounded"))
          parentRigidBody.velocity= new Vector3(animator.deltaPosition.x/Time.deltaTime, parentRigidBody.velocity.y, animator.deltaPosition.z / Time.deltaTime);
    }

}
