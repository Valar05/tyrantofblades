﻿using UnityEngine;
using System.Collections;

public class ResetOnEnter : StateMachineBehaviour {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.GetComponent<PlayerController>().ResetAttack();
    }

}
