﻿using UnityEngine;
using System.Collections;

public class ResetDamagedOnExit : StateMachineBehaviour {

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.ResetTrigger("Damaged");
        animator.ResetTrigger("KnockUp");
    }

}