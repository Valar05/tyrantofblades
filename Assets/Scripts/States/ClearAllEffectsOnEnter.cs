﻿using UnityEngine;
using System.Collections;

public class ClearAllEffectsOnEnter : StateMachineBehaviour {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        PlayerController player = animator.gameObject.GetComponent<PlayerController>();
        player.EndWeaponTrail();
        player.EndActiveFrame();
        Time.timeScale = 1f;
    }

}
