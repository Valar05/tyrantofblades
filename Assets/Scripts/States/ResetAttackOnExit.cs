﻿using UnityEngine;
using System.Collections;

public class ResetAttackOnExit : StateMachineBehaviour {
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        PlayerController player = animator.gameObject.GetComponent<PlayerController>();
        player.EndAttack();
        player.ResetLaunch();
        animator.ResetTrigger("LightAttack");
        animator.ResetTrigger("HeavyAttack");
        animator.ResetTrigger("SpecialAttack");
        animator.ResetTrigger("HeavyAttackHold");
        animator.ResetTrigger("Parry");
        animator.ResetTrigger("TapStick");
        animator.SetBool("IsFinisher", false);
        animator.ResetTrigger("HeavyAttackShort");
    }
}
