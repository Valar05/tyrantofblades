﻿using SimpleJSON;
using UnityEngine;

namespace Assets.Scripts.States {
    [System.Serializable]
    public class CharacterState {
        public string name;
        public int damage;
        public int invulLevel;
        public float shakeAmount;
        public float castSize;
        public float slowTimeScale;
        public float knockBackForce;
        public bool isRootMotion;
        public bool actionEnabled;
        public bool isLauncher;
        public bool isMeteorSmash;
        public bool isSelfLauncher;
        public bool damaged;
        public bool isFallFaster;
        public bool isBlocking;
        public float launchSpeed;
        public float launchTime;
        public bool isAttack;
        public int hitDirHash;
        public bool isCancellable;

        public CharacterState(JSONNode data) {
            name = data["name"];
            damage = data["damage"] != null ? data["damage"].AsInt : 0;
            invulLevel = data["invulLevel"] != null ? data["invulLevel"].AsInt : 0;
            shakeAmount = data["shakeAmount"] != null ? data["shakeAmount"].AsFloat : .6f;
            castSize = data["castSize"] != null ? data["castSize"].AsFloat : 0.5f;
            slowTimeScale = data["slowTimeScale"] != null ? data["slowTimeScale"].AsFloat : 0.2f;
            knockBackForce = data["knockBackForce"] != null ? data["knockBackForce"].AsFloat : 0f;
            isRootMotion = data["isRootMotion"] != null ? data["isRootMotion"].AsBool : false;
            actionEnabled = data["actionEnabled"] != null ? data["actionEnabled"].AsBool : true;
            isLauncher = data["isLauncher"] != null ? data["isLauncher"].AsBool : false;
            isMeteorSmash = data["isMeteorSmash"] != null ? data["isMeteorSmash"].AsBool : false;
            isSelfLauncher = data["isSelfLauncher"] != null ? data["isSelfLauncher"].AsBool : false;
            damaged = data["damaged"] != null ? data["damaged"].AsBool : false;
            isFallFaster = data["isFallFaster"] != null ? data["isFallFaster"].AsBool : false;
            isBlocking = data["isBlocking"] != null ? data["isBlocking"].AsBool : false;
            launchSpeed = data["launchSpeed"] != null ? data["launchSpeed"].AsFloat : 0f;
            launchTime = data["launchTime"] != null ? data["launchTime"].AsFloat : 0f;
            isAttack = data["isAttack"] != null ? data["isAttack"].AsBool : false;
            hitDirHash = data["hitDir"] != null ? Animator.StringToHash(data["hitDir"].Value) : Animator.StringToHash("Damaged");
            isCancellable = data["isCancellable"] != null ? data["isCancellable"].AsBool : false;
        }
    }
}
