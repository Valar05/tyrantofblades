﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts {
    static class GameUtils {
    
        public static GameObject FindInChild(Transform[] transforms, string name) {
            foreach (Transform t in transforms) {
                if (t.name == name)
                    return t.gameObject;
            }
            return null;
        }
    }
}
