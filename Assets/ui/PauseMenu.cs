﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public GameObject pauseCanvas;

    Canvas pauseMenu;
    PlayerController player;

    bool paused;
    float prevTimeScale;

	void Start () {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        pauseCanvas.SetActive(true);
        pauseMenu = pauseCanvas.GetComponent<Canvas>();
        pauseMenu.enabled = false;
	}
	
	void Update() {
        if ((Input.GetButtonDown("Cancel"))) {
            paused = !paused;
            if (paused) {
                pauseMenu.enabled = true;
                prevTimeScale = Time.timeScale;
                Time.timeScale = 0;
                player.enabled = false;
            }
            if (!paused) {
                player.enabled = true;
                pauseMenu.enabled = false;
                Time.timeScale = prevTimeScale;
            }
        }
    }

    public void ResumeClick() {
        paused = false;
        player.enabled = true;
        pauseMenu.enabled = false;
        Time.timeScale = prevTimeScale;
    }

    public void ExitClick() {
        SceneManager.LoadScene(0);
    }
}
